
import java.util.ArrayList;

public class Piece {
	enum color{
		white,
		black
	}
	enum type{
		pawn,
		rook,
		knight,
		bishop,
		queen,
		king
	}
	enum designation{
		P,R,k,B,Q,K,E
	}
	private color piece_color ;
	private type piece_type;
	private Tuple<Integer,Integer> location;
	designation piece_desig;
	private boolean moved; 
	ArrayList<Tuple<Integer,Integer>> moves;
	Integer direction;
	type get_type() {
		return this.piece_type;
	}
	void set_moved(boolean a) {
		this.moved = a;
	}
	boolean get_moved() {
		return this.moved;
	}
	void set_type(type t) {
		if (this instanceof Pawn) {
			this.piece_type = Piece.type.pawn;
			this.piece_desig = Piece.designation.P;
		} else if (this instanceof Rook) {
			this.piece_type = Piece.type.rook;
			this.piece_desig = Piece.designation.R;
		} else if (this instanceof Knight) {
			this.piece_type  = Piece.type.knight;
			this.piece_desig = Piece.designation.k;
		} else if (this instanceof Bishop) {
			this.piece_type  = Piece.type.bishop;
			this.piece_desig = Piece.designation.B;
		} else if (this instanceof King) {
			this.piece_type  = Piece.type.king;
			this.piece_desig = Piece.designation.K;
		} else if (this instanceof Queen) {
			this.piece_type  = Piece.type.queen;
			this.piece_desig = Piece.designation.Q;
		} else {
			this.piece_type  = null;
			this.piece_desig = null;	
		}
	}
	void set_color(color c) {
		this.piece_color  = c;
	}
	color get_color() {
		return this.piece_color;
	}
	void set_location(Tuple<Integer,Integer> location) {
		this.location = location;
	}
	Tuple<Integer,Integer> get_location() {
		return this.location;
	}
	Piece(color c, type t, designation d, Tuple<Integer,Integer> l){
		this.piece_color  = c;
		this.piece_type = t;
		this.piece_desig = d;
		this.location = l;
	}
	Piece(Tuple<Integer,Integer> location){
		this.piece_color = null;
		this.piece_type = null;
		this.piece_desig = null;
		this.location = null;
	}
	Piece(){
		this.piece_color = null;
		this.piece_type = null;
		this.piece_desig = null;
		this.location = null;
	}
	
	void print() {
		if (this instanceof Pawn || this instanceof Rook ||this instanceof Knight ||
			  this instanceof Bishop || this instanceof Queen || this instanceof King) {
			if (this.piece_color == color.black){
				System.out.print(this.piece_desig + " ");
			} else if (this.piece_color == color.white) {
				System.err.print(this.piece_desig + " ");
			} 
		} else {
			System.out.print("_ ");
		}
	}
	
	void move(Board board, Tuple<Integer,Integer> pos) {
		if (board.get_piece(2,3) instanceof Pawn &&
				((Pawn) board.get_piece(2,3)).is_valid(pos, board) == false) {
			// throw error;
			
		}
		// call is_valid() to verify move
		// move piece
		// set moved
		
		// swap pieces if position isn't occupied; i.e null
		// if position is occupied by another position capture or throw err
		Piece n; 
		Tuple<Integer,Integer> p ;
		if ( board.board.get(pos.toString()).piece_type == null) {
			// get the target squares piece
			n = board.get_piece(pos.x, pos.y);
			// save off the location
			p = this.location;
			// move the current piece to the location
			board.set_piece(this, pos.x, pos.y);
			this.set_moved(true);
			// move the null square over to the old space
			board.set_piece(n, p.x, p.y);
			
		} else if ( board.board.get(pos.toString()).piece_color != this.piece_color) {
			// get the target squares piece
			n = board.get_piece(pos.x, pos.y);
			// save off the location
			p = this.location;
			// move the current piece to the location
			board.set_piece(this, pos.x, pos.y);
			this.set_moved(true);
			// move a new null square over to the old space
			board.set_piece(new Piece(), p.x, p.y);
			
		} else {
			
		}
	}
}
