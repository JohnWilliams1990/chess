

public class Rook extends Piece {
	Integer value;
	Rook(){
		this.set_type(Piece.type.rook);
		this.piece_desig = Piece.designation.R;
	}
	Rook(color c){
		this.set_type(Piece.type.rook);
		this.piece_desig = Piece.designation.R;
		this.set_color(c);
		this.set_moved(false);
	}
	void moves(Board board, Tuple<Integer,Integer> position) {
		
	}
}
