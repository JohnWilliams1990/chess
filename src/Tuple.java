public class Tuple<X, Y> { 
  public final X x; 
  public final Y y; 
  public Tuple(X x, Y y) { 
    this.x = x; 
    this.y = y; 
  } 
  public void print() {
	  System.out.print("(" + this.x + "," + this.y + ") " );
  }
  Tuple<Integer, Integer> distance_a(Tuple<Integer, Integer> a) {
	   return new Tuple<Integer, Integer>(Math.abs(a.x - (int) this.x), Math.abs(a.y - (int) this.y));
	  //return new Tuple<X, Y>(Math.abs(a.x - Integer.valueOf((String) this.x)), Math.abs(a.y - Integer.valueOf((String) this.y)));
 }
  Tuple<Integer, Integer> distance(Tuple<Integer, Integer> a) {
	   return new Tuple<Integer, Integer>(a.x - (int) this.x, a.y - (int) this.y);
	  //return new Tuple<X, Y>(Math.abs(a.x - Integer.valueOf((String) this.x)), Math.abs(a.y - Integer.valueOf((String) this.y)));
  }
  Tuple<Integer, Integer> distance(Tuple<Integer, Integer> a, Tuple<Integer, Integer> b) {
	  return new Tuple<Integer, Integer>(Math.abs(a.x - b.x), Math.abs(a.y - b.y));
  }
  @Override
  public String toString() {
	  return "(" + this.x + "," + this.y + ")";
  }
  @Override
  public int hashCode()
  {
      return this.toString().hashCode();
  }

  @Override
  public boolean equals(Object o)
  {
      return this.toString().equals(o.toString());
  }
} 