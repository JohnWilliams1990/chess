
public class Knight extends Piece {
	Integer value;
	Knight(){
		this.set_type(Piece.type.knight);
		this.piece_desig = Piece.designation.k;
	}
	Knight (color c){
		this.set_type(Piece.type.knight );
		this.piece_desig = Piece.designation.k;
		this.set_color(c);
		this.set_moved(false);
	}
	void moves(Board board, Tuple<Integer,Integer> position) {
		
	}
}
