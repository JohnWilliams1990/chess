public class Queen extends Piece {
	Integer value;
	Queen(){
		this.set_type(Piece.type.queen);
		this.piece_desig = Piece.designation.Q;
	}
	Queen(color c){
		this.set_type(Piece.type.queen);
		this.piece_desig = Piece.designation.Q;
		this.set_color(c);
		this.set_moved(false);
	}
	void moves(Board board, Tuple<Integer,Integer> position) {
		
	}
}
