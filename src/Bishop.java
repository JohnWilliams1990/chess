

public class Bishop extends Piece {
	Integer value;
	Bishop(){
		this.set_type(Piece.type.bishop);
		this.piece_desig = Piece.designation.B;
	}
	Bishop (color c){
		this.set_type(Piece.type.bishop);
		this.piece_desig = Piece.designation.B;
		this.set_color(c);
		this.set_moved(false);
	}
	void moves(Board board, Tuple<Integer,Integer> position) {
		
	}
}