public class King extends Piece {
	Integer value;
	King(){
		this.set_type(Piece.type.king);
		this.piece_desig = Piece.designation.K;
	}
	King (color c){
		this.set_type(Piece.type.king);
		this.piece_desig = Piece.designation.K;
		this.set_color(c);
		this.set_moved(false);
	}
	void moves(Board board, Tuple<Integer,Integer> position) {
		
	}
}
