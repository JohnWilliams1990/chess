import java.util.ArrayList;

public class Pawn extends Piece {
	Integer direction;
	Integer value;
	Pawn(){
		this.set_type(Piece.type.pawn);
		this.piece_desig = Piece.designation.P;
	}
	Pawn(color c){
		this.set_type(Piece.type.pawn);
		this.piece_desig = Piece.designation.P;
		this.set_color(c);
	}
	Pawn(color c, Integer dir){
		this.set_type(Piece.type.pawn);
		this.piece_desig = Piece.designation.P;
		this.set_color(c);
		this.direction = dir;
		this.set_moved(false);
	}
	boolean is_valid (Tuple<Integer,Integer> a, Board b) {
		if (this.get_location() == null) {
			return false;
		} else if (this.get_moved() == true) {
			if (this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 1,0)) &&
				b.get_piece(a.x, a.y).get_type() == null) {
				return true;
			} else if ((this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 1,1)) || 
				this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 1,-1))) &&
				b.get_piece(a.x, a.y).get_color() != this.get_color() &&
				b.get_piece(a.x, a.y).get_color() != null) {
				return true;
			}
		} else {
			if (this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 1,0)) ||
				this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 2,0)) &&
				b.get_piece(a.x, a.y).get_type() == null) {
				return true;
			} else if ((this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 1,1)) || 
				this.get_location().distance(a).equals(new Tuple<Integer,Integer>(this.direction * 1,-1))) &&
				b.get_piece(a.x, a.y).get_color() != this.get_color() &&
				b.get_piece(a.x, a.y).get_color() != null) {
				return true;
			}
		}
		return false;
	}
	boolean is_valid (Tuple<Integer,Integer> a, Tuple<Integer,Integer> b) {
		if (this.get_moved()) {
			
		} else {
			
		}
		return false;
	}
	
	ArrayList<Tuple<Integer,Integer>> valid_moves; // = ArrayList<Tuple<Integer,Integer>>
	
}
