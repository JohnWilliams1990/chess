
import java.util.Hashtable;

import Piece.color;

public class Board {
	int dim_x = 8;
	Hashtable<String,Piece> board = new Hashtable<String,Piece>();
	Hashtable<String,Integer> place = new Hashtable<String,Integer>();
	void set_piece(Piece p, int i, int j){
		p.set_location(new Tuple<Integer,Integer>(i,j));
		this.board.put(new Tuple<Integer,Integer>(i,j).toString(),p);
	}
	Piece get_piece(int i, int j){
		return this.board.get(new Tuple<Integer,Integer>(i,j).toString());
	}
	Board(){
		this.place.put("A",7);
		place.put("B",6);
		place.put("C",5);
		place.put("D",4);
		place.put("E",3);
		place.put("F",2);
		place.put("G",1);
		place.put("H",0);
		for (int i = 0; i < this.dim_x; ++i) {
			for (int j = 0; j < this.dim_x; ++j) {
				//Piece piece = new Piece();
				board.put(new Tuple<Integer,Integer>(i,j).toString(),new Piece());
			}
		}
	}
	
	void move(int x_f, int y_f, int x_t, int y_t) {
		this.board.get(new Tuple<Integer,Integer>(x_f,y_f).toString()).move(this, new Tuple<Integer,Integer>(x_t,y_t));
		this.print();
	}
	void move(String fx, String fy, String tx, String ty ) {
		this.board.get(new Tuple<Integer,Integer>(place.get(fx),place.get(fy)).toString()).move(this, new Tuple<Integer,Integer>(place.get(tx),place.get(ty)));
		this.print();
	}
	void move(String fx, Integer fy, String tx, Integer ty ) {
		fy = 7 - fy;
		ty = 7 - ty;
		this.board.get(new Tuple<Integer,Integer>(place.get(fx),fy).toString()).move(this, new Tuple<Integer,Integer>(place.get(tx),ty));
		this.print();
	}
	void print() {
		for (int i = 0; i < this.dim_x; ++i) {
			for (int j = 0; j < this.dim_x; ++j) {
				this.board.get(new Tuple<Integer,Integer>(i,j).toString()).print();
			}
			System.out.println();
		}
		System.out.println();
	}
	void set(Piece.color c) {
		Piece.color P1 ;
		Piece.color P2 ;			

		if (c == Piece.color.white) {
			P1 = Piece.color.white;
			P2 = Piece.color.black;	
			this.set_piece(new King(P1), 0, 3);
			this.set_piece(new Queen(P1), 0, 4);
			this.set_piece(new King(P2), 7, 3);
			this.set_piece(new Queen(P2), 7, 4);
		} else {
			P1 = Piece.color.black;			
			P2 = Piece.color.white;
			this.set_piece(new King(P1), 0, 4);
			this.set_piece(new Queen(P1), 0, 3);
			this.set_piece(new King(P2), 7, 4);
			this.set_piece(new Queen(P2), 7, 3);
		}
		
		this.set_piece(new Rook(P1), 0, 0);
		this.set_piece(new Rook(P1), 0, 7);
		this.set_piece(new Rook(P2), 7, 0);
		this.set_piece(new Rook(P2), 7, 7);
	
		this.set_piece(new Knight(P1), 0, 1);
		this.set_piece(new Knight(P1), 0, 6);
		this.set_piece(new Knight(P2), 7, 1);
		this.set_piece(new Knight(P2), 7, 6);
		
		this.set_piece(new Bishop(P1), 0, 2);
		this.set_piece(new Bishop(P1), 0, 5);
		this.set_piece(new Bishop(P2), 7, 2);
		this.set_piece(new Bishop(P2), 7, 5);
		
		for (int i = 0; i < this.dim_x; ++i) {
			this.set_piece(new Pawn(P1,1), 1, i);
			this.set_piece(new Pawn(P2,-1), 6, i);
		}
		for (int i = 0; i < this.dim_x; ++i) {
			for (int j = 0; j < this.dim_x; ++j) {
				if (this.get_piece(i, j) == null) {
					this.set_piece(new Piece(), i, j);
				}
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Board board = new Board();
		//board.set(Piece.color.black);
		board.set(Piece.color.white);
		board.print();
		Piece p = board.board.get(new Tuple<Integer,Integer>(0,1).toString());
		
		board.move(0,1, 2,2);
		board.move(1,3, 2,3);
		board.move(1,3, 2,3);
		
		for (int i = 1; i < board.dim_x; ++i) {
			board.move(1, i, 2, i);
		}
		board.print();
		
	}

}


/*  
1.  
 Add boarder to console board (A-H)
  A B C D E F G H
 A
 B
 C
 D
 E
 F
 G
 H
 2. Make the board's move function have a lookup table for 
 
 A --> 1
 B --> 2
 C --> 3
 D --> 4
 E --> 5
 F --> 6
 G --> 7
 H --> 8

3. define valid moves for the individual classes. 
4. start looking at the means of refactoring the existing code
5. define parameters of the outstanding game
6. make a game class to run the game
7. start looking at AI solutions for Pruning

 
 */


/*
System.out.println(new Tuple<Integer,Integer>(1,2).distance(new Tuple<Integer,Integer>(6,4), new Tuple<Integer,Integer>(3,2)).toString());
System.out.println(new Tuple<Integer,Integer>(1,2).distance(new Tuple<Integer,Integer>(0,4)).toString());
System.out.println(new Tuple<Integer,Integer>(1,3).distance(new Tuple<Integer,Integer>(2,3)).toString());
System.out.println(((Pawn) board.get_piece(1,1)).is_valid(new Tuple<Integer,Integer>(1,1), board));

System.out.println(((Pawn) board.get_piece(2,3)).is_valid(new Tuple<Integer,Integer>(3,3), board));
System.out.println(((Pawn) board.get_piece(2,3)).is_valid(new Tuple<Integer,Integer>(3,4), board));

Tuple<Integer,Integer> piece = new Tuple<Integer,Integer>(1,2);
for (int i = 0; i < board.dim_x; ++i) {
	for (int j = 0; j < board.dim_x; ++j) {
		if (board.get_piece(1,4) instanceof Pawn &&
			((Pawn) board.get_piece(1,4)).is_valid(new Tuple<Integer,Integer>(i,j), board) == true) {
			
			System.out.println(new Tuple<Integer,Integer>(i,j));
			//System.out.println(((Pawn) board.get_piece(1,1)).is_valid(new Tuple<Integer,Integer>(i,j), board));
		}
	}
}
*/